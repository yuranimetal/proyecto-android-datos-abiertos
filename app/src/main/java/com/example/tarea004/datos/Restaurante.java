package com.example.tarea004.datos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Restaurante {

    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("entidad_cargo")
    @Expose
    private String entidadCargo;
    @SerializedName("direccion")
    @Expose
    private String direccion;
    @SerializedName("correo")
    @Expose
    private String correo;
    @SerializedName("telefono")
    @Expose
    private String telefono;
    @SerializedName("latitud")
    @Expose
    private String latitud;
    @SerializedName("longitud")
    @Expose
    private String longitud;
    @SerializedName("geocoded_column")
    @Expose
    private GeocodedColumn geocodedColumn;


    public Restaurante(String nombre, String entidadCargo,
                       String direccion, String correo, String telefono,
                       String latitud, String longitud, GeocodedColumn geocodedColumn) {
        super();
        this.nombre = nombre;
        this.entidadCargo = entidadCargo;
        this.direccion = direccion;
        this.correo = correo;
        this.telefono = telefono;
        this.latitud = latitud;
        this.longitud = longitud;
        this.geocodedColumn = geocodedColumn;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEntidadCargo() {
        return entidadCargo;
    }

    public void setEntidadCargo(String entidadCargo) {
        this.entidadCargo = entidadCargo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public GeocodedColumn getGeocodedColumn() {
        return geocodedColumn;
    }

    public void setGeocodedColumn(GeocodedColumn geocodedColumn) {
        this.geocodedColumn = geocodedColumn;
    }
}