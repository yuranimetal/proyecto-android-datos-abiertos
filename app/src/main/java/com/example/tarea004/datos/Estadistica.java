package com.example.tarea004.datos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Estadistica {

    @SerializedName("poblaci_n")
    @Expose
    private String poblaciN;
    @SerializedName("cantidad")
    @Expose
    private String cantidad;
    @SerializedName("masculino")
    @Expose
    private String masculino;
    @SerializedName("femenino")
    @Expose
    private String femenino;
    @SerializedName("_17_a_20_a_os")
    @Expose
    private String _17A20AOs;
    @SerializedName("_21a30_a_os")
    @Expose
    private String _21a30AOs;
    @SerializedName("_31_a_40_a_os")
    @Expose
    private String _31A40AOs;
    @SerializedName("_41_a_50_a_os")
    @Expose
    private String _41A50AOs;
    @SerializedName("_51_a_60_a_os")
    @Expose
    private String _51A60AOs;
    @SerializedName("_61_a_70_a_os")
    @Expose
    private String _61A70AOs;
    @SerializedName("_71_a_80_a_os")
    @Expose
    private String _71A80AOs;

    /**
     * No args constructor for use in serialization
     *
     */
    public Estadistica() {
    }

    /**
     *
     * @param poblaciN
     * @param femenino
     * @param _31A40AOs
     * @param _41A50AOs
     * @param _21a30AOs
     * @param masculino
     * @param _51A60AOs
     * @param cantidad
     * @param _61A70AOs
     * @param _71A80AOs
     * @param _17A20AOs
     */
    public Estadistica(String poblaciN, String cantidad, String masculino, String femenino, String _17A20AOs, String _21a30AOs, String _31A40AOs, String _41A50AOs, String _51A60AOs, String _61A70AOs, String _71A80AOs) {
        super();
        this.poblaciN = poblaciN;
        this.cantidad = cantidad;
        this.masculino = masculino;
        this.femenino = femenino;
        this._17A20AOs = _17A20AOs;
        this._21a30AOs = _21a30AOs;
        this._31A40AOs = _31A40AOs;
        this._41A50AOs = _41A50AOs;
        this._51A60AOs = _51A60AOs;
        this._61A70AOs = _61A70AOs;
        this._71A80AOs = _71A80AOs;
    }

    public String getPoblaciN() {
        return poblaciN;
    }

    public void setPoblaciN(String poblaciN) {
        this.poblaciN = poblaciN;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getMasculino() {
        return masculino;
    }

    public void setMasculino(String masculino) {
        this.masculino = masculino;
    }

    public String getFemenino() {
        return femenino;
    }

    public void setFemenino(String femenino) {
        this.femenino = femenino;
    }

    public String get17A20AOs() {
        return _17A20AOs;
    }

    public void set17A20AOs(String _17A20AOs) {
        this._17A20AOs = _17A20AOs;
    }

    public String get21a30AOs() {
        return _21a30AOs;
    }

    public void set21a30AOs(String _21a30AOs) {
        this._21a30AOs = _21a30AOs;
    }

    public String get31A40AOs() {
        return _31A40AOs;
    }

    public void set31A40AOs(String _31A40AOs) {
        this._31A40AOs = _31A40AOs;
    }

    public String get41A50AOs() {
        return _41A50AOs;
    }

    public void set41A50AOs(String _41A50AOs) {
        this._41A50AOs = _41A50AOs;
    }

    public String get51A60AOs() {
        return _51A60AOs;
    }

    public void set51A60AOs(String _51A60AOs) {
        this._51A60AOs = _51A60AOs;
    }

    public String get61A70AOs() {
        return _61A70AOs;
    }

    public void set61A70AOs(String _61A70AOs) {
        this._61A70AOs = _61A70AOs;
    }

    public String get71A80AOs() {
        return _71A80AOs;
    }

    public void set71A80AOs(String _71A80AOs) {
        this._71A80AOs = _71A80AOs;
    }
}