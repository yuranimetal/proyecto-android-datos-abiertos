package com.example.tarea004.datos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeptMunCol {

    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("c_digo_dane_del_departamento")
    @Expose
    private String cDigoDaneDelDepartamento;
    @SerializedName("departamento")
    @Expose
    private String departamento;
    @SerializedName("c_digo_dane_del_municipio")
    @Expose
    private String cDigoDaneDelMunicipio;
    @SerializedName("municipio")
    @Expose
    private String municipio;

    /**
     * No args constructor for use in serialization
     *
     */
    public DeptMunCol() {
    }

    /**
     *
     * @param cDigoDaneDelMunicipio
     * @param municipio
     * @param departamento
     * @param cDigoDaneDelDepartamento
     * @param region
     */
    public DeptMunCol(String region, String cDigoDaneDelDepartamento, String departamento, String cDigoDaneDelMunicipio, String municipio) {
        super();
        this.region = region;
        this.cDigoDaneDelDepartamento = cDigoDaneDelDepartamento;
        this.departamento = departamento;
        this.cDigoDaneDelMunicipio = cDigoDaneDelMunicipio;
        this.municipio = municipio;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCDigoDaneDelDepartamento() {
        return cDigoDaneDelDepartamento;
    }

    public void setCDigoDaneDelDepartamento(String cDigoDaneDelDepartamento) {
        this.cDigoDaneDelDepartamento = cDigoDaneDelDepartamento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCDigoDaneDelMunicipio() {
        return cDigoDaneDelMunicipio;
    }

    public void setCDigoDaneDelMunicipio(String cDigoDaneDelMunicipio) {
        this.cDigoDaneDelMunicipio = cDigoDaneDelMunicipio;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

}