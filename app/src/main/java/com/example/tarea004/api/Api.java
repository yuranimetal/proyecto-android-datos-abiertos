package com.example.tarea004.api;

import com.example.tarea004.datos.DeptMunCol;
import com.example.tarea004.datos.Estadistica;
import com.example.tarea004.datos.Restaurante;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;


public interface Api {
    @GET
    Call<List<DeptMunCol>> getDeptMunColList(@Url String url);
    @GET
    Call<List<Restaurante>> getRestauranteList(@Url String url);
    @GET
    Call<List<Estadistica>> getEstadisticaList(@Url String url);
}
