package com.example.tarea004.ui.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.tarea004.R;
import com.example.tarea004.ui.Fragments.DeptMunCol.DeptMunColFragment;

public class AcercaDeFragment extends Fragment {
    private Bundle bn;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_acerca_de, container, false);

        bn = savedInstanceState;
        root.findViewById(R.id.btn_atras).setOnClickListener(view -> regresar(view));

        return root;
    }

    public void regresar(View view) {
        DeptMunColFragment calculadoraFragment = new DeptMunColFragment();
        calculadoraFragment.setArguments(bn);

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.nav_host_fragment, calculadoraFragment)
                .commit();
    }
}