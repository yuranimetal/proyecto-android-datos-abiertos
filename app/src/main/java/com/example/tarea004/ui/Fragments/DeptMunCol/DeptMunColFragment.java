package com.example.tarea004.ui.Fragments.DeptMunCol;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tarea004.R;
import com.example.tarea004.api.Api;
import com.example.tarea004.datos.DeptMunCol;

/* import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.constants.OpenStreetMapTileProviderConstants;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;*/

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeptMunColFragment extends Fragment {

    private View root;
    private Bundle bn;

    private Retrofit retrofit;
    private final static String TAG = "RESTAURANTE.ERROR ---| ";

    private RecyclerView rv;

    @RequiresApi(api = Build.VERSION_CODES.N)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_dept_mun_col, container, false);
        bn = savedInstanceState;

        rv = (RecyclerView)root.findViewById(R.id.rv);
        rv.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(root.getContext());
        rv.setLayoutManager(llm);

        retrofit = new Retrofit.Builder().baseUrl("https://www.datos.gov.co/resource/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getData();

        return root;
    }

    private void getData() {
        try{

            Api service = retrofit.create(Api.class);
            Call<List<DeptMunCol>> deptMunColResponseCall = service.getDeptMunColList("xdk5-pm3f.json");

            deptMunColResponseCall.enqueue(new Callback<List<DeptMunCol>>() {
                @Override
                public void onResponse(Call<List<DeptMunCol>> call, Response<List<DeptMunCol>> response) {
                    if(response.isSuccessful()){
                        List<DeptMunCol> deptMunColsList = response.body();
                        DeptMunColRVAdapter adapter = new DeptMunColRVAdapter(deptMunColsList);
                        rv.setAdapter(adapter);
                    } else {
                        Log.e(TAG, "onResponse: "+response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<List<DeptMunCol>> call, Throwable t) {
                    Log.e(TAG," onFailure: "+t.getStackTrace());
                }
            });

        }catch (Exception e){
            Log.e(TAG, "onFailure: " + e);
        }

    }

}