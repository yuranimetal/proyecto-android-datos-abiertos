package com.example.tarea004.ui.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.example.tarea004.R;
import com.example.tarea004.api.Api;
import com.example.tarea004.datos.Restaurante;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestauranteFragment extends Fragment {
    private View root;

    private Retrofit retrofit;
    private final static String TAG = "RESTAURANTE.ERROR ---| ";

    @RequiresApi(api = Build.VERSION_CODES.N)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_restaurante, container, false);

        retrofit = new Retrofit.Builder().baseUrl("https://www.datos.gov.co/resource/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync( googleMap -> getData(googleMap) );

        return root;
    }

    private void getData(GoogleMap googleMap) {
        try{
            Api service = retrofit.create(Api.class);
            Call<List<Restaurante>> restauranteResponseCall = service.getRestauranteList("sym6-cfhq.json");

            restauranteResponseCall.enqueue(new Callback<List<Restaurante>>() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onResponse(Call<List<Restaurante>> call, Response<List<Restaurante>> response) {
                    if(response.isSuccessful()) {
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(1.2136100, -77.2811100), 14
                        ));
                        List<Restaurante> restaurantes = response.body();
                        for(Restaurante r : restaurantes)  {
                            addNewMark(googleMap, r.getLatitud(), r.getLongitud(), r.getEntidadCargo(), R.drawable.restaurant);
                        }
                    } else {
                        Log.e(TAG, "onResponse: "+response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<List<Restaurante>> call, Throwable t) {
                    Log.e(TAG," onFailure: "+t.getStackTrace());
                }
            });

        }catch (Exception e){
            Log.e(TAG, "onFailure: " + e);
        }
    }

    private void addNewMark(GoogleMap gM, String lat, String lon, String t, Integer i) {
        LatLng latLng = new LatLng(
                Double.valueOf(lat),
                Double.valueOf(lon));
        gM.addMarker(
                new MarkerOptions().position(latLng).title(t)
                        .icon(BitmapDescriptorFactory.fromResource(i))
        );
    }
}
