package com.example.tarea004.ui.Fragments.Estadistica;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Column;
import com.anychart.enums.Anchor;
import com.anychart.enums.HoverMode;
import com.anychart.enums.Position;
import com.anychart.enums.TooltipPositionMode;
import com.example.tarea004.R;
import com.example.tarea004.datos.Estadistica;

import java.util.Arrays;


public class TabEstadisticaFragment extends Fragment {
    private Estadistica e;

    public TabEstadisticaFragment(Estadistica e) {
        this.e = e;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.estadistica_tab, container, false);

        TextView cantidadPer = root.findViewById(R.id.cantidad_per);
        // AnyChartView grafFM = root.findViewById(R.id.graf_fm);
        AnyChartView grafAnios = root.findViewById(R.id.graf_anios);

        cantidadPer.setText(e.getCantidad());

        /*addAnyChart(grafFM, "Datos por genero", "Genero", "# Personas",
                new ValueDataEntry("Femenino", Integer.valueOf(e.getFemenino())),
                new ValueDataEntry("Masculino", Integer.valueOf(e.getMasculino())));*/

        addAnyChart(grafAnios, "Datos por rango de Edades", "Rango de edad", "# Personas",
            new ValueDataEntry("17 - 20", Integer.valueOf(e.get17A20AOs())),
            new ValueDataEntry("21 - 30", Integer.valueOf(e.get21a30AOs())),
            new ValueDataEntry("31 - 40", Integer.valueOf(e.get31A40AOs())),
            new ValueDataEntry("41 - 50", Integer.valueOf(e.get41A50AOs())),
            new ValueDataEntry("51 - 60", Integer.valueOf(e.get51A60AOs())),
            new ValueDataEntry("61 - 70", Integer.valueOf(e.get61A70AOs())),
            new ValueDataEntry("71 - 80", Integer.valueOf(e.get71A80AOs())));

        return root;
    }

    private void addAnyChart(AnyChartView graf, String title, String xLabel, String yLabel, DataEntry...data) {
        Cartesian cartesian = AnyChart.column();
        Column column = cartesian.column(Arrays.asList(data));

        column.tooltip()
                .titleFormat("{%X}")
                .position(Position.CENTER_BOTTOM)
                .anchor(Anchor.CENTER_BOTTOM)
                .offsetX(0d)
                .offsetY(5d)
                .format("{%Value}{groupsSeparator: }");

        cartesian.animation(true);
        cartesian.title(title);

        cartesian.yScale().minimum(0d);

        cartesian.yAxis(0).labels().format("{%Value}{groupsSeparator: }");

        cartesian.tooltip().positionMode(TooltipPositionMode.POINT);
        cartesian.interactivity().hoverMode(HoverMode.BY_X);

        cartesian.xAxis(0).title(xLabel);
        cartesian.yAxis(0).title(yLabel);

        graf.setChart(cartesian);
    }
}