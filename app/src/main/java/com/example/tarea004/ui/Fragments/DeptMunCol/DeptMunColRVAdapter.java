package com.example.tarea004.ui.Fragments.DeptMunCol;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.tarea004.R;
import com.example.tarea004.datos.DeptMunCol;

import java.util.List;

public class DeptMunColRVAdapter extends RecyclerView.Adapter<DeptMunColRVAdapter.DeptMunColViewHolder> {
    List<DeptMunCol> deptMunCol;

    public DeptMunColRVAdapter(List<DeptMunCol> deptMunCol){
        this.deptMunCol = deptMunCol;
    }

    @Override
    public int getItemCount() {
        return deptMunCol.size();
    }

    @Override
    public DeptMunColViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card, viewGroup, false);
        DeptMunColViewHolder rvh = new DeptMunColViewHolder(v);
        return rvh;
    }

    @Override
    public void onBindViewHolder(DeptMunColViewHolder deptMunColViewHolder, int i) {
        DeptMunCol item = deptMunCol.get(i);
        deptMunColViewHolder.titulo.setText(
                item.getDepartamento().concat(" - ").concat(item.getMunicipio()));
        deptMunColViewHolder.region.setText(deptMunCol.get(i).getRegion());
        deptMunColViewHolder.codDept.setText(deptMunCol.get(i).getCDigoDaneDelDepartamento());
        deptMunColViewHolder.codMun.setText(deptMunCol.get(i).getCDigoDaneDelMunicipio());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class DeptMunColViewHolder extends RecyclerView.ViewHolder {
        TextView titulo;
        TextView region;
        TextView codDept;
        TextView codMun;

        DeptMunColViewHolder(View itemView) {
            super(itemView);
            titulo = itemView.findViewById(R.id.titulo);
            region = itemView.findViewById(R.id.region);
            codDept = itemView.findViewById(R.id.cod_dept);
            codMun = itemView.findViewById(R.id.cod_mun);
        }
    }

}
