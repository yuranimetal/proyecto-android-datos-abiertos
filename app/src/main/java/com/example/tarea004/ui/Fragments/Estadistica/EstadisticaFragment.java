package com.example.tarea004.ui.Fragments.Estadistica;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.tarea004.R;
import com.example.tarea004.api.Api;
import com.example.tarea004.datos.Estadistica;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EstadisticaFragment extends Fragment {
    private View root;

    private EstadisticaAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private Retrofit retrofit;
    private final static String TAG = "RESTAURANTE.ERROR ---| ";

    @RequiresApi(api = Build.VERSION_CODES.N)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_estadistica, container, false);

        retrofit = new Retrofit.Builder().baseUrl("https://www.datos.gov.co/resource/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        viewPager = (ViewPager) root.findViewById(R.id.viewPager); // donde se ponen los tabs
        tabLayout = (TabLayout) root.findViewById(R.id.tabLayout);

        getData();

        return root;
    }

    private void getData() {
        try{
            Api service = retrofit.create(Api.class);
            Call<List<Estadistica>> estadisticaResponseCall = service.getEstadisticaList("ktfh-rxyz.json");

            estadisticaResponseCall.enqueue(new Callback<List<Estadistica>>() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onResponse(Call<List<Estadistica>> call, Response<List<Estadistica>> response) {
                    if(response.isSuccessful()) {
                        List<Estadistica> estadiscas = response.body();
                        if(estadiscas.size()>0) {
                            adapter = new EstadisticaAdapter(getFragmentManager());

                            for(Estadistica e : estadiscas)  {
                                adapter.addFragment(new TabEstadisticaFragment(e), e.getPoblaciN());
                            }

                            viewPager.setAdapter(adapter);
                            tabLayout.setupWithViewPager(viewPager);
                        }
                    } else {
                        Log.e(TAG, "onResponse: "+response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<List<Estadistica>> call, Throwable t) {
                    Log.e(TAG," onFailure: "+t.getStackTrace());
                }
            });

        }catch (Exception e){
            Log.e(TAG, "onFailure: " + e);
        }
    }
}
